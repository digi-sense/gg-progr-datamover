# Pre-Builds #

### Linux ###

 - linux: Should work on most release. Use more specific builds below if you get problems.
 - linux/amd64
 - linux/386
 - linux/arm
 - linux/arm64

### Mac ###

- darwin/amd64: Intel chip
- darwin/arm64: M1 chip

### Windows ###

- windows/amd64
- windows/386


Download from [HERE](https://mega.nz/folder/61hwEboA#ZD6KRd4HgZVxBqKlLoRhpA)