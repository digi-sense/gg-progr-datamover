package action

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_fnvars"
	"bitbucket.org/digi-sense/gg-core/gg_log"
	"bitbucket.org/digi-sense/gg-progr-datamover/datamover/datamover_commons"
	"bitbucket.org/digi-sense/gg-progr-datamover/datamover/datamover_jobs/action/schema"
	"bitbucket.org/digi-sense/gg-progr-datamover/datamover/datamover_jobs/action/scripting"
	"fmt"
)

type DataMoverDatasource struct {
	root               string
	uid                string
	autoUpdateSchema   bool
	fnVarEngine        *gg_fnvars.FnVarsEngine
	logger             gg_log.ILogger
	logLevel           gg_log.Level
	connectionSettings *datamover_commons.DataMoverConnectionSettings
	scriptContext      string
	scriptBefore       string
	scriptAfter        string
	initialized        bool
	schemaUpdated      bool

	_connection  *Connection
	schema       *schema.DataMoverDatasourceSchema
	scriptEngine *scripting.ScriptController
}

func NewDataMoverDatasource(root string, fnVarEngine *gg_fnvars.FnVarsEngine, logger gg_log.ILogger,
	connection *datamover_commons.DataMoverConnectionSettings,
	scripts *datamover_commons.DataMoverActionScriptSettings) (*DataMoverDatasource, error) {

	instance := new(DataMoverDatasource)
	instance.root = root
	instance.uid = gg.Coding.MD5(root)
	instance.autoUpdateSchema = Registry.PutDatasource(instance.uid)
	instance.fnVarEngine = fnVarEngine
	instance.logger = logger
	instance.logLevel = datamover_commons.GetLogLevel(logger)

	instance.connectionSettings = connection
	if nil != instance.connectionSettings {
		instance.schema = instance.connectionSettings.Schema
	}
	if nil != scripts {
		instance.scriptBefore = scripts.Before
		instance.scriptContext = scripts.Context
		instance.scriptAfter = scripts.After
	}

	// err := instance.init()

	return instance, nil
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *DataMoverDatasource) Uid() string {
	if nil != instance {
		return instance.uid
	}
	return ""
}

func (instance *DataMoverDatasource) GetSchema() *schema.DataMoverDatasourceSchema {
	if nil != instance {
		_ = instance.init() // late initialize
		return instance.schema
	}
	return nil
}

func (instance *DataMoverDatasource) GetData(command string, fieldsMapping map[string]interface{}, context []map[string]interface{}, variables map[string]interface{}) ([]map[string]interface{}, error) {
	result := make([]map[string]interface{}, 0)
	scriptVars := make(map[string]interface{})

	instance.logger.Debug(fmt.Sprintf("datasource.GetData(): command='%s', fieldsMapping='%s', context='%s', variables='%s', ",
		command, gg.JSON.Stringify(fieldsMapping), gg.JSON.Stringify(context), gg.JSON.Stringify(variables)))

	initErr := instance.init() // late initialize
	if nil != initErr {
		return nil, initErr
	}

	conn := instance.connection()
	db, err := conn.Get()
	if nil != err {
		Pool.Close(conn) // close and remove connection
		// query error
		err = gg.Errors.Prefix(err, fmt.Sprintf("Database Connection Error with DSN '%s' -> ",
			instance.connectionSettings.Dsn))
		return nil, err
	}

	//
	if nil == context {
		// solve fnvars
		instance.logger.Debug(fmt.Sprintf("datasource.GetData()#before#SolveCommandSpecialVars: command='%s', variables='%s'", command, gg.JSON.Stringify(variables)))
		command = SolveCommandSpecialVars(instance.fnVarEngine, command, variables)
		instance.logger.Debug(fmt.Sprintf("datasource.GetData()#after#SolveCommandSpecialVars: command='%s', variables='%s'", command, gg.JSON.Stringify(variables)))

		// before retrieve data
		if len(instance.scriptBefore) > 0 {
			instance.logger.Debug(fmt.Sprintf("datasource.GetData()#script#BEFORE:  variables='%s'", gg.JSON.Stringify(variables)))
			result, scriptVars = instance.scriptEngine.RunWithArray("before", instance.scriptBefore, result, variables)
			if len(scriptVars) > 0 {
				gg.Maps.Merge(true, variables, scriptVars)
			}
		}

		// retrieve data
		statement := ValidateSqlStatement(command)
		if len(statement) > 0 {
			result, err = query(db, statement, variables)
			if nil != err {
				Pool.Close(conn)
				err = gg.Errors.Prefix(err, "Data Query Error -> ")
				return nil, err
			}
		} else {
			instance.logger.Debug(fmt.Sprintf("Found invalid statement '%s'", command))
		}

		// context script
		if len(instance.scriptContext) > 0 {
			instance.logger.Debug(fmt.Sprintf("datasource.GetData()#script#CONTEXT:  variables='%s'", gg.JSON.Stringify(variables)))
			result, scriptVars = instance.scriptEngine.RunWithArray("context", instance.scriptContext, result, variables)
			if len(scriptVars) > 0 {
				gg.Maps.Merge(true, variables, scriptVars)
			}
		}

		// after all
		if len(instance.scriptAfter) > 0 {
			instance.logger.Debug(fmt.Sprintf("datasource.GetData()#script#AFTER:  variables='%s'", gg.JSON.Stringify(variables)))
			result, scriptVars = instance.scriptEngine.RunWithArray("after", instance.scriptAfter, result, variables)
			if len(scriptVars) > 0 {
				gg.Maps.Merge(true, variables, scriptVars)
			}
		}

	} else {

		// before retrieve data
		if len(instance.scriptBefore) > 0 {
			instance.logger.Debug(fmt.Sprintf("datasource.GetData()#script#BEFORE:  variables='%s'", gg.JSON.Stringify(variables)))
			context, scriptVars = instance.scriptEngine.RunWithArray("before", instance.scriptBefore, context, variables)
			if len(scriptVars) > 0 {
				gg.Maps.Merge(true, variables, scriptVars)
			}
		}

		// context script
		if len(instance.scriptContext) > 0 {
			instance.logger.Debug(fmt.Sprintf("datasource.GetData()#script#CONTEXT:  variables='%s'", gg.JSON.Stringify(variables)))
			context, scriptVars = instance.scriptEngine.RunWithArray("context", instance.scriptContext, context, variables)
			if len(scriptVars) > 0 {
				gg.Maps.Merge(true, variables, scriptVars)
			}
		}

		// add items to result
		for _, data := range context {
			if nil != data {
				ctx := gg.Maps.Merge(false, map[string]interface{}{}, data, variables)
				// solve fnvars
				instance.logger.Debug(fmt.Sprintf("datasource.GetData()#before#SolveCommandSpecialVars: command='%s', variables='%s'", command, gg.JSON.Stringify(ctx)))
				command = SolveCommandSpecialVars(instance.fnVarEngine, command, ctx)
				instance.logger.Debug(fmt.Sprintf("datasource.GetData()#after#SolveCommandSpecialVars: command='%s', variables='%s'", command, gg.JSON.Stringify(ctx)))

				statement := ToSQLStatement(command, data, fieldsMapping)
				if len(statement) > 0 {
					// retrieve row data
					r, e := query(db, statement, ctx)
					if nil != e {
						Pool.Close(conn)
						e = gg.Errors.Prefix(err, fmt.Sprintf("Database Connection Error with STATEMENT '%s' -> ",
							statement))
						return nil, e
					}

					if len(r) == 0 {
						result = append(result, data)
					} else {
						result = append(result, r...)
					}
				} else {
					instance.logger.Debug(fmt.Sprintf("Found invalid statement '%s'", command))
				}
			}
		}

		// after
		if len(instance.scriptAfter) > 0 {
			instance.logger.Debug(fmt.Sprintf("datasource.GetData()#script#AFTER:  variables='%s'", gg.JSON.Stringify(variables)))
			result, scriptVars = instance.scriptEngine.RunWithArray("after", instance.scriptAfter, result, variables)
			if len(scriptVars) > 0 {
				gg.Maps.Merge(true, variables, scriptVars)
			}
		}
	}
	return result, nil
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

// initialize the schema, network, etc...
func (instance *DataMoverDatasource) init() (err error) {
	if nil != instance && !instance.initialized {

		err = instance.initSchema()
		if nil != err {
			err = gg.Errors.Prefix(err, "Init Schema Error -> ")
			return err
		}
		instance.scriptEngine = scripting.NewScriptController(instance.root, instance.logLevel)

		instance.initialized = true
	}
	return
}

func (instance *DataMoverDatasource) initSchema() error {
	conn := instance.connection()
	defer Pool.Close(conn)
	db, err := conn.Get()
	if nil != err {
		return err
	}
	if nil == instance.schema {
		// AUTO-CREATE SCHEMA
		instance.schema = schema.NewSchema()
		tbls, err := db.Migrator().GetTables()
		if nil != err {
			return err
		}
		for _, tbl := range tbls {
			tx := db.Table(tbl)
			s := &struct{}{}
			cols, e := tx.Migrator().ColumnTypes(&s)
			if nil != e {
				return e
			}
			// add table to schema
			table := schema.NewTable()
			table.Name = tbl
			for _, col := range cols {
				column := &schema.DataMoverDatasourceSchemaColumn{
					Name: col.Name(),
				}
				column.Nullable, _ = col.Nullable()
				column.Type = col.DatabaseTypeName()
				table.Columns = append(table.Columns, column)
			}
			instance.schema.Tables = append(instance.schema.Tables, table)
			// fmt.Println(table)
		}
	} else {
		// AUTO-MIGRATE SCHEMA
		if instance.autoUpdateSchema && !instance.schemaUpdated {
			instance.schemaUpdated = true
			tables := instance.schema.Tables
			for _, table := range tables {
				if !db.Migrator().HasTable(table.Name) {
					tx := db.Raw("CREATE TABLE ?", table.Name)
					if nil != tx.Error {
						return tx.Error
					}
				}
				s := table.Struct()
				e := db.Table(table.Name).Migrator().AutoMigrate(s)
				if nil != e {
					return e
				}
			}
		}
	}
	return nil
}

// retrieve a connection
func (instance *DataMoverDatasource) connection() *Connection {
	if nil != instance {
		driver := instance.connectionSettings.Driver
		dsn := instance.connectionSettings.Dsn
		conn := Pool.Get(instance.root, driver, dsn)
		instance.logger.Debug(fmt.Sprintf("datasource.connection(): uid='%s', root='%s', driver='%s', dsn='%s'",
			conn.uid, conn.root, conn.driver, conn.dsn))
		return conn
	}
	return nil
}
