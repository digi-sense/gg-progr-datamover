package action

import "time"

var Registry *DataRegister

func init() {
	Registry = new(DataRegister)
	Registry.dataSources = make(map[string]*dataSource)
}

type dataSource struct {
	Uid       string
	Timestamp time.Time
}

type DataRegister struct {
	dataSources map[string]*dataSource
}

func (instance *DataRegister) HasDatasource(uid string) bool {
	if nil != instance && nil != instance.dataSources {
		if _, ok := instance.dataSources[uid]; ok {
			return true
		}
		return false
	}
	return true
}

func (instance *DataRegister) PutDatasource(uid string) bool {
	if nil != instance && !instance.HasDatasource(uid) {
		instance.dataSources[uid] = &dataSource{
			Uid:       uid,
			Timestamp: time.Now(),
		}
		return true
	}
	return false
}
