package action

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-progr-datamover/datamover/datamover_commons"
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/driver/postgres"
	"gorm.io/driver/sqlite"
	"gorm.io/driver/sqlserver"
	"gorm.io/gorm"
	"sync"
)

var Pool *Connections

type Connections struct {
	connections map[string]*Connection
	mux         sync.Mutex
}

func (instance *Connections) Get(root, driver, dsn string) *Connection {
	instance.mux.Lock()
	defer instance.mux.Unlock()
	uid := gg.Coding.MD5(root + driver + dsn)
	if conn, ok := instance.connections[uid]; !ok || nil == conn {
		instance.connections[uid] = NewConnection(root, driver, dsn)
	}
	return instance.connections[uid]
}

func (instance *Connections) Close(item interface{}) {
	instance.mux.Lock()
	defer instance.mux.Unlock()
	var uid string
	if s, ok := item.(string); ok {
		uid = s
	} else if c, ok := item.(*Connection); ok {
		uid = c.uid
	}
	if len(uid) > 0 {
		if _, ok := instance.connections[uid]; ok {
			conn := instance.connections[uid]
			instance.connections[uid] = nil
			_ = conn.Close()
		}
	}
}

func init() {
	Pool = &Connections{
		connections: make(map[string]*Connection),
	}
}

type Connection struct {
	uid    string
	root   string
	driver string
	dsn    string
	_db    *gorm.DB
}

func NewConnection(root, driver, dsn string) *Connection {
	instance := new(Connection)
	instance.uid = gg.Coding.MD5(root + driver + dsn)
	instance.root = root
	instance.driver = driver
	instance.dsn = dsn
	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *Connection) Close() error {
	if nil != instance && nil != instance._db {
		db, err := instance._db.DB()
		instance._db = nil
		if nil != err {
			return err
		}
		return db.Close()
	}
	return nil
}

func (instance *Connection) Get() (*gorm.DB, error) {
	var err error
	var db *gorm.DB
	root := instance.root
	driver := instance.driver
	dsn := instance.dsn
	if nil == instance._db {
		switch driver {
		case "sqlite":
			filename := gg.Paths.Concat(root, dsn)
			db, err = gorm.Open(sqlite.Open(filename), &gorm.Config{})
		case "mysql":
			// "user:pass@tcp(127.0.0.1:3306)/dbname?charset=utf8mb4&parseTime=True&loc=Local"
			db, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})
		case "postgres":
			// "host=localhost user=gorm password=gorm dbname=gorm port=9920 sslmode=disable TimeZone=Asia/Shanghai"
			db, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})
		case "sqlserver":
			// "sqlserver://gorm:LoremIpsum86@localhost:9930?database=gorm"
			db, err = gorm.Open(sqlserver.Open(dsn), &gorm.Config{})
		default:
			db = nil
			err = gg.Errors.Prefix(datamover_commons.DatabaseNotSupportedError,
				fmt.Sprintf("'%s': ", driver))
		}
		if nil == err {
			instance._db = db
		}
	}

	return instance._db, err
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------
