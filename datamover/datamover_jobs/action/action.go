package action

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_fnvars"
	"bitbucket.org/digi-sense/gg-core/gg_log"
	"bitbucket.org/digi-sense/gg-progr-datamover/datamover/datamover_commons"
	"bitbucket.org/digi-sense/gg-progr-datamover/datamover/datamover_globals"
	"bitbucket.org/digi-sense/gg-progr-datamover/datamover/datamover_jobs/action/schema"
	"bitbucket.org/digi-sense/gg-progr-datamover/datamover/datamover_network/clients"
	"bitbucket.org/digi-sense/gg-progr-datamover/datamover/datamover_network/message"
	"fmt"
	"strings"
	"sync"
)

type DataMoverAction struct {
	root    string
	uid     string
	execMux sync.Mutex // synchronize the execution to avoid thread issues

	actionSettings *datamover_commons.DataMoverActionSettings
	datasource     *DataMoverDatasource
	_clientNet     clients.ClientNetwork
	globals        *datamover_globals.Globals

	network *datamover_commons.DataMoverNetworkSettings
}

func NewDataMoverAction(root string, fnVarEngine *gg_fnvars.FnVarsEngine, logger gg_log.ILogger, datasourceSettings *datamover_commons.DataMoverActionSettings, globals *datamover_globals.Globals) (instance *DataMoverAction, errors []error) {
	instance = new(DataMoverAction)
	instance.root = root
	instance.actionSettings = datasourceSettings
	instance.globals = globals
	var err error
	if nil != datasourceSettings {
		instance.uid = datasourceSettings.Uid

		// network
		instance.network = datasourceSettings.Network
		if nil != instance.network && len(instance.network.NetworksId) > 0 && instance.globals.HasNetworks() {
			net := instance.globals.GetNetwork(instance.network.NetworksId)
			if nil != net {
				instance.network = net // replace with globals
			}
		}

		// connection
		connection := datasourceSettings.Connection
		if len(connection.ConnectionsId) > 0 && instance.globals.HasConnections() {
			// replace with global connection
			conn := instance.globals.GetConnection(connection.ConnectionsId)
			if nil != conn {
				connection = conn
			}
		}
		instance.datasource, err = NewDataMoverDatasource(root, fnVarEngine, logger,
			connection, datasourceSettings.Scripts)
		if nil != err {
			errors = append(errors, err)
		}

		// init the action
		err = instance.init()
		if nil != err {
			errors = append(errors, err)
		}
	}
	// action can return some errors for missing connections
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *DataMoverAction) Uid() string {
	if nil != instance {
		return instance.uid
	}
	return ""
}

func (instance *DataMoverAction) IsValid() bool {
	return nil != instance.datasource
}

func (instance *DataMoverAction) IsNetworkAction() bool {
	return nil != instance.actionSettings &&
		nil != instance.network && // nil != instance.actionSettings.Network &&
		len(instance.network.Host) > 0 // len(instance.actionSettings.Network.Host) > 0
}

func (instance *DataMoverAction) GetSchema() *schema.DataMoverDatasourceSchema {
	if nil != instance && nil != instance.datasource {
		return instance.datasource.GetSchema()
	}
	return nil
}

func (instance *DataMoverAction) Execute(contextData []map[string]interface{}, variables map[string]interface{}) (result []map[string]interface{}, err error) {
	if nil != instance {
		instance.execMux.Lock()
		defer instance.execMux.Unlock()

		result = make([]map[string]interface{}, 0)

		if instance.IsNetworkAction() {
			// ------------------------
			// REMOTE
			// ------------------------
			client, e := instance.getClientNet()
			if nil != client {
				payload := new(message.NetworkMessagePayload)
				payload.ActionName = "net-command"
				payload.ActionRoot = NormalizePathForOS(instance.root)
				payload.ActionRootRelative = strings.ReplaceAll(payload.ActionRoot, NormalizePathForOS(gg.Paths.WorkspacePath("")), ".")
				payload.ActionConfig = instance.actionSettings
				payload.ActionContextData = contextData
				payload.ActionContextVariables = variables
				payload.ActionGlobals = instance.globals
				payload.ActionDatasets = LoadJsDatasets(instance.root) // load datasets for remote transfer

				// check again scripts are loaded
				_ = payload.ActionConfig.ScriptsLoad(instance.root)

				// execute
				respData, respErr := client.Send(payload.String())
				if nil != respErr {
					if strings.HasPrefix(respErr.Error(), "dial tcp") {
						instance._clientNet = nil // reset client
					}
					err = respErr
					return
				}
				// deserialize
				var res *message.NetworkMessageResponseBody
				str := gg.Convert.ToString(respData)
				err = gg.JSON.Read(str, &res)
				if nil == err {
					// align datasets
					if nil != res.Datasets && len(res.Datasets) > 0 {
						OverwriteJsDatasets(instance.root, res.Datasets)
					}
					// align variables
					gg.Maps.Merge(true, variables, res.Variables)
					// read the body
					err = gg.JSON.Read(gg.Convert.ToString(res.Body), &result)
				}
			} else {
				err = e
			}
			if nil != err {
				// check error is a valid error and not simply an empty dataset
				if datamover_commons.IsValidError(err) {
					err = gg.Errors.Prefix(err,
						fmt.Sprintf("Remote Execution Error form Action '%s': ", instance.uid),
					)
				} else {
					err = nil
				}
			}
		} else {
			// LOCAL
			command := instance.actionSettings.Command
			if len(command) > 0 {
				mapping := instance.actionSettings.FieldsMapping
				result, err = instance.datasource.GetData(command, mapping, contextData, instance.globals.InjectConstantsIntoVariables(variables))
			}
			if nil != err {
				if datamover_commons.IsValidError(err) {
					err = gg.Errors.Prefix(err, fmt.Sprintf("Local Execution Error form Action '%s': ", instance.uid))
				} else {
					err = nil
				}
			}
		}
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *DataMoverAction) init() (err error) {
	if nil != instance && nil != instance.datasource {
		// check network or test database connection
		if instance.IsNetworkAction() {
			_, err = instance.getClientNet() // test connection
			if nil != err {
				err = gg.Errors.Prefix(err, fmt.Sprintf("Network Connection test Error ('%s') -> ", instance.network.Host))
				return
			}
		} else {
			conn := instance.datasource.connection()
			defer Pool.Close(conn)
			_, err = conn.Get()
			if nil != err {
				err = gg.Errors.Prefix(err, fmt.Sprintf("Database Connection test Error ('%s') -> ", conn.dsn))
				return
			}
		}
	} else {
		err = gg.Errors.Prefix(datamover_commons.PanicSystemError, "MISSING DATASOURCE")
	}
	return
}

func (instance *DataMoverAction) getClientNet() (clients.ClientNetwork, error) {
	if instance.IsNetworkAction() && nil == instance._clientNet {
		host := instance.network.Host
		c, e := clients.BuildNetworkClient(host, instance.network)
		if nil != e {
			e = gg.Errors.Prefix(e, "Network Connection Error -> ")
			instance._clientNet = nil
			return instance._clientNet, e
		}
		instance._clientNet = c
	}
	return instance._clientNet, nil
}
