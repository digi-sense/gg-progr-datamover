package datamover

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_events"
	"bitbucket.org/digi-sense/gg-core/gg_ticker"
	"fmt"
	"sync"
	"time"
)

// ---------------------------------------------------------------------------------------------------------------------
//		t y p e
// ---------------------------------------------------------------------------------------------------------------------

type actionMonitor struct {
	roots       []string // where stop file is stored
	actionCmd   string
	actionEvent string
	events      *gg_events.Emitter
	fileMux     sync.Mutex
	ticker      *gg_ticker.Ticker
}

// ---------------------------------------------------------------------------------------------------------------------
//		c o n s t r u c t o r
// ---------------------------------------------------------------------------------------------------------------------

func newActionMonitor(roots []string, actionCmd, actionEvent string, events *gg_events.Emitter) *actionMonitor {
	instance := new(actionMonitor)
	instance.roots = roots
	instance.actionCmd = actionCmd
	instance.actionEvent = actionEvent
	instance.events = events

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//		p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *actionMonitor) Start() {
	if nil != instance && len(instance.actionCmd) > 0 && nil == instance.ticker {
		instance.ticker = gg_ticker.NewTicker(1*time.Second, func(t *gg_ticker.Ticker) {
			instance.checkAction()
			// instance.logger.Debug("Checking for stop command....")
		})
		instance.ticker.Start()
	}
}

func (instance *actionMonitor) Stop() {
	if nil != instance && nil != instance.ticker {
		instance.ticker.Stop()
		instance.ticker = nil
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//		p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *actionMonitor) checkAction() {
	if nil != instance {
		if len(instance.actionCmd) > 0 {
			instance.fileMux.Lock()
			defer instance.fileMux.Unlock()

			cmd := instance.actionCmd

			// check if file exists
			for _, root := range instance.roots {
				cmdFile := gg.Paths.Concat(root, cmd)
				if b, _ := gg.Paths.Exists(cmdFile); b {
					_ = gg.IO.Remove(cmdFile)
					instance.events.EmitAsync(instance.actionEvent)
				}
			}
		}
		instance.tick()
	}
}

func (instance *actionMonitor) tick() {
	root := gg.Paths.WorkspacePath("")
	filename := gg.Paths.Concat(root, "datamover.tick")
	_, _ = gg.IO.WriteTextToFile(fmt.Sprintf("%v", time.Now()), filename)
}
