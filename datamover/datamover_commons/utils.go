package datamover_commons

import (
	"bitbucket.org/digi-sense/gg-core/gg_log"
	"strings"
)

func IsValidError(err error) bool {
	if strings.Index(err.Error(), "unexpected end of JSON") == -1 {
		return false
	}
	return true
}

func GetLogLevel(logger gg_log.ILogger) gg_log.Level {
	if l, b := logger.(*gg_log.Logger); b {
		return l.GetLevel()
	}
	return gg_log.InfoLevel
}
