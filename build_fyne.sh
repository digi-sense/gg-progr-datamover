##!/bin/sh

BASE="1.1"
# Get latest build
BUILD=$(<build_version.txt)
chrlen=${#BUILD}
if [ $chrlen = 0 ]
then
  BUILD=0
fi

echo "START BUILDING WITH FYNE-CROSS (https://github.com/fyne-io/fyne-cross) $BASE.$BUILD..."

# windows
sh ./build_fyne_windows.sh

# linux
sh ./build_fyne_linux.sh

# mac
sh ./build_fyne_mac.sh

echo "remove temp files ..."
rm -r ./fyne-cross